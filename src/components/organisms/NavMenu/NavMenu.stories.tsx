/* eslint-disable */
import React from "react";
import { storiesOf } from "@storybook/react";
import { NavMenu } from "./NavMenu";

const container = undefined;
const theme = { direction: "rtl" };
const mobileOpen = false;
const handleDrawerToggle = () => console.log("test");
const classes = {
  drawerPaper: "test",
};

storiesOf("NavMenu", module).add("default", () => (
  <NavMenu
    container={container}
    theme={theme}
    mobileOpen={mobileOpen}
    handleDrawerToggle={handleDrawerToggle}
    classes={classes}
  />
));
