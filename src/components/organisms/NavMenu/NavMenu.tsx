import { Drawer, Hidden } from "@material-ui/core";
import React from "react";
import styles from "./NavMenu.module.scss";

interface Props {
  container: (() => HTMLElement) | undefined;
  theme: {
    direction: string;
  };
  mobileOpen: boolean;
  handleDrawerToggle: () => void;
  classes: {
    drawerPaper: string;
  };
}

export const NavMenu: React.FC<Props> = ({
  container,
  theme,
  mobileOpen,
  handleDrawerToggle,
  classes,
  children,
}) => (
  <div className={styles.NavMenu} data-testid="NavMenu">
    <Hidden smUp implementation="css">
      <Drawer
        container={container}
        variant="temporary"
        anchor={theme.direction === "rtl" ? "right" : "left"}
        open={mobileOpen}
        onClose={handleDrawerToggle}
        classes={{
          paper: classes.drawerPaper,
        }}
        ModalProps={{
          keepMounted: true, // Better open performance on mobile.
        }}
      >
        {children}
      </Drawer>
    </Hidden>
    <Hidden xsDown implementation="css">
      <Drawer
        classes={{
          paper: classes.drawerPaper,
        }}
        variant="permanent"
        open
      >
        {children}
      </Drawer>
    </Hidden>
  </div>
);
