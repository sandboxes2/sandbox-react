import React from "react";
import { render, screen } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import { NavMenu } from "./NavMenu";

describe("<NavMenu />", () => {
  test("it should mount", () => {
    const container = undefined;
    const theme = { direction: "rtl" };
    const mobileOpen = false;
    const handleDrawerToggle = () => console.log("test");
    const classes = {
      drawerPaper: "test",
    };

    render(
      <NavMenu
        container={container}
        theme={theme}
        mobileOpen={mobileOpen}
        handleDrawerToggle={handleDrawerToggle}
        classes={classes}
      />
    );

    const navMenu = screen.getByTestId("NavMenu");

    expect(navMenu).toBeInTheDocument();
  });
});
