import React from "react";
import { render, screen } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import { NavBody } from "./NavBody";

describe("<NavBody />", () => {
  test("it should mount", () => {
    const classes = {
      content: "test1",
      toolbar: "test2",
    };

    render(<NavBody classes={classes} />);

    const navBody = screen.getByTestId("NavBody");

    expect(navBody).toBeInTheDocument();
  });
});
