/* eslint-disable */
import React from "react";
import { storiesOf } from "@storybook/react";
import { NavBody } from "./NavBody";

const classes = {
  content: "test1",
  toolbar: "test2",
};

storiesOf("NavBody", module).add("default", () => (
  <NavBody classes={classes} />
));
