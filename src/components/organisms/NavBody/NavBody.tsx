import { Typography } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import styles from "./NavBody.module.scss";
import axios, { AxiosResponse } from "axios";
import Display from "../../../component/organisms/Display/Display";

interface Props {
  classes: {
    content: string;
    toolbar: string;
  };
}

export interface Data {
  title: string;
}

export const NavBody: React.FC<Props> = ({ classes }) => {
  const [fakeData, setFakeData] = useState<Data>({
    title: "",
  });

  useEffect(() => {
    const fetchFakeData = async () => {
      const result: AxiosResponse<Data> = await axios(
        "http://localhost/api/test"
      );

      setFakeData(result.data);
    };

    fetchFakeData();
  }, []);

  return (
    <div className={styles.NavBody} data-testid="NavBody">
      <main className={classes.content}>
        <div className={classes.toolbar} />
        <Typography paragraph>
          <Display title={fakeData.title} value={10} required={true} />
        </Typography>
      </main>
    </div>
  );
};
