/* eslint-disable */
import React from "react";
import { storiesOf } from "@storybook/react";
import { NavHeader } from "./NavHeader";

const classes = {
  appBar: "test1",
  menuButton: "test2",
};

const handleDrawerToggle = () => console.log("test");

storiesOf("NavHeader", module).add("default", () => (
  <NavHeader classes={classes} handleDrawerToggle={handleDrawerToggle} />
));
