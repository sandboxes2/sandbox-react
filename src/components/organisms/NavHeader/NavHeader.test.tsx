import React from "react";
import { render, screen } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import { NavHeader } from "./NavHeader";

describe("<NavHeader />", () => {
  test("it should mount", () => {
    const classes = {
      appBar: "test1",
      menuButton: "test2",
    };

    const handleDrawerToggle = () => console.log("test");

    render(
      <NavHeader classes={classes} handleDrawerToggle={handleDrawerToggle} />
    );

    const navHeader = screen.getByTestId("NavHeader");

    expect(navHeader).toBeInTheDocument();
  });
});
