import { AppBar, IconButton, Toolbar, Typography } from "@material-ui/core";
import React from "react";
import styles from "./NavHeader.module.scss";
import MenuIcon from "@material-ui/icons/Menu";

interface Props {
  classes: {
    appBar: string;
    menuButton: string;
  };
  handleDrawerToggle: () => void;
}

export const NavHeader: React.FC<Props> = ({ classes, handleDrawerToggle }) => (
  <div className={styles.NavHeader} data-testid="NavHeader">
    <AppBar position="fixed" className={classes.appBar}>
      <Toolbar>
        <IconButton
          color="inherit"
          aria-label="open drawer"
          edge="start"
          onClick={handleDrawerToggle}
          className={classes.menuButton}
        >
          <MenuIcon />
        </IconButton>
        <Typography variant="h6" noWrap>
          Responsive drawer
        </Typography>
      </Toolbar>
    </AppBar>
  </div>
);
