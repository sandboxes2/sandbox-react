/* eslint-disable */
import React from "react";
import { storiesOf } from "@storybook/react";
import { NavDrawer } from "./NavDrawer";

const classes = {
  toolbar: "test",
};

storiesOf("NavDrawer", module).add("default", () => (
  <NavDrawer classes={classes} />
));
