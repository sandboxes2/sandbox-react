import React from "react";
import styles from "./Display.module.scss";

interface Props {
  title: string;
  value: number;
  required: boolean;
}

const Display: React.FC<Props> = ({ title, value, required = false }) => {
  let display;

  if (required) {
    display = `Le titre est : "${title}"`;
  } else {
    display =
      value < 5
        ? `J'affiche les ${value} éléments`
        : `J'affiche les 5 premiers éléments sur ${value}`;
  }
  return (
    <div className={styles.Display} data-testid="Display">
      {display}
    </div>
  );
};

export default Display;
