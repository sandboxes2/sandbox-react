/* eslint-disable */
import React from "react";
import { storiesOf } from "@storybook/react";
import Display from "./Display";

storiesOf("Display", module).add("default", () => (
  <Display value={10} title={"My title"} required={true} />
));
